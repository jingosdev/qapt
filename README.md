# qapt
qapt is based on libqapt [github](https://github.com/KDE/libqapt) , a beautifully designed qapt that conforms to the JingOS style and has a compatible `pad / desktop`  experience.

qapt is a convergent qapt application built with the [controlkit](https://github.com/JingOS-team/controlkit). Although it is mainly targeted for  desktop.

Originally starting as a fork of [qapt](https://github.com/JingOS-team/qapt), qapt has gone through heavy development, and no longer shares the same codebase with  qapt.


## Links
* Home: www.jingos.com
* Project page: https://invent.kde.org/jingosdev/qapt
* File issues: https://invent.kde.org/jingosdev/qapt/issues
* Development channel:  www.jingos.com

## Installing
This will compile and install qapt onto the system. When running qapt, make sure that qapt is running first (it is configured to autostart in sessions).

```
mkdir build
cd build
cmake ..
make
sudo make install
```


## qapt
The qapt daemon, which is configured to auto start on Plasma launch (located in the src folder). It has the following functions:
debian  package install tool

